import React from 'react';
import '../styles/footer.css'
import { Link } from 'react-router-dom'

const Footer = props => {
  return (
    <>

      <footer className="page-footer font-small">


        <div className="container">

          <div className="row">


            <div className="col-md-12 py-5">
              <div className="mb-5 flex-center">


                <Link className="fb-ic">
                  <i className="fab fa-facebook-f fa-lg white-text mr-md-5 mr-3 fa-2x"> </i>
                </Link>

                <Link className="tw-ic">
                  <i className="fab fa-twitter fa-lg white-text mr-md-5 mr-3 fa-2x"> </i>
                </Link>

                <Link className="gplus-ic">
                  <i className="fab fa-google-plus-g fa-lg white-text mr-md-5 mr-3 fa-2x"> </i>
                </Link>

                <Link className="li-ic">
                  <i className="fab fa-linkedin-in fa-lg white-text mr-md-5 mr-3 fa-2x"> </i>
                </Link>

                <Link className="ins-ic">
                  <i className="fab fa-instagram fa-lg white-text mr-md-5 mr-3 fa-2x"> </i>
                </Link>

                <Link className="pin-ic">
                  <i className="fab fa-pinterest fa-lg white-text fa-2x"> </i>
                </Link>
              </div>
            </div>


          </div>


        </div>

        <div className="footer-copyright text-center py-3">© 2020 Copyright:
    <Link href="https://mdbootstrap.com/"> MDBootstrap.com</Link>
        </div>

      </footer>

    </>
  )
}
export default Footer;